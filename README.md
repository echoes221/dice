#Dice

Dice roller implementation takes either dice notation or numbered notation as specified by the task.

I'm making some assumptions (in the wake of time) about the inputs:
 - All inputs are valid format wise
 - Integers are used
 - No negative numbers (apart from modifiers)

## Install & Usage

```
$ npm install
```
Then require the folder locally as needed. Only `roll` and `createRoller` will be exposed.

## Testing
```
$ npm run test
```


