'use strict';

const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const dice = require('../src/dice');

describe('dice', () => {
    describe('Roller', () => {
        describe('Roller constructor', () => {
            describe('when it recieves dice notation', () => {
                it('should set the dice values correctly', () => {
                    const roller = new dice.Roller(['1d5-3']);

                    expect(roller.diceValues).to.deep.equal({
                        diceCount: 1,
                        faceCount: 5,
                        modifier: -3
                    });
                });
            });

            describe('when it recieves number notation', () => {
                it('should set the dice values correctly', () => {
                    const roller = new dice.Roller([1, 5, '-3']);

                    expect(roller.diceValues).to.deep.equal({
                        diceCount: 1,
                        faceCount: 5,
                        modifier: -3
                    });
                });
            });

            describe('when it recieves no values', () => {
                it('should set the default values', () => {
                    const roller = new dice.Roller();

                    expect(roller.diceValues).to.deep.equal({
                        diceCount: 1,
                        faceCount: 6,
                        modifier: 0
                    });
                });
            });

            describe('constructor return function', () => {
                it('should return a function', () => {
                    const roller = new dice.Roller();

                    expect(typeof roller).to.equal('function');
                });

                it('should throw the dice when called', () => {
                    // Should normally spy this, but as it's not a dependency,
                    // its harder to test
                    const roller = new dice.Roller();
                    expect(roller()).to.be.an('number');
                });
            });
        });

        // TODO this should loop through several values to test but time...
        describe('toDiceNotation', () => {
            it('should return diceNotation from diceNotation', () => {
                const notation = '1d5+3';
                const notation2 = '1d5-3'
                const roller = new dice.Roller([notation]);
                const roller2 = new dice.Roller([notation2])

                expect(roller.toDiceNotation()).to.equal(notation);
                expect(roller2.toDiceNotation()).to.equal(notation2);
            });

            it('should return diceNotation from number notation', () => {
                const notation = [2, 6, '-4'];
                const notation2 = [2, 6, '+4'];
                const roller = new dice.Roller(notation);
                const roller2 = new dice.Roller(notation2);

                expect(roller.toDiceNotation()).to.equal('2d6-4');
                expect(roller2.toDiceNotation()).to.equal('2d6+4');
            });
        });
    });

    describe('createRoller', () => {
        it('should return a new instance of Roller', () => {
            expect(dice.createRoller()).to.be.instanceof(dice.Roller);
        });
    });

    describe('roll', () => {
        beforeEach(() => {
            sinon.stub(Math, 'random').returns(0.23079);
        });

        afterEach(() => {
            Math.random.restore();
        });

        describe('When given diceNotation', () => {
            it('should roll the dice', () => {
                expect(dice.roll('5d8+3')).to.equal(25);
            });
        });

        describe('When given numberNotation', () => {
            it('should roll the dice', () => {
                expect(dice.roll(5, 8, 3)).to.equal(25);
            });
        });
    });

    describe('identifyInput', () => {
        describe('When given a string', () => {
            it('should return diceNotation function', () => {
                expect(dice.identifyInput('foo').toString()).to.equal(dice.diceNotation.toString());
            });
        });

        describe('When given a number value', () => {
            it('should return numberNotation function', () => {
                expect(dice.identifyInput(1).toString()).to.equal(dice.numberNotation.toString());
            });
        });
    });

    describe('diceNotation', () => {
        describe('When given no values', () => {
            it('should return the default values', () => {
                expect(dice.diceNotation()).to.deep.equal({
                    diceCount: 1,
                    faceCount: 6,
                    modifier: 0
                });
            });
        });

        describe('When given diceNotation', () => {
            it('should return complete override of defaults if diceNotation is whole', () => {
                expect(dice.diceNotation('2d7-4')).to.deep.equal({
                    diceCount: 2,
                    faceCount: 7,
                    modifier: -4
                });
            });

            it('should only override the diceCount when provided', () => {
                expect(dice.diceNotation('3d')).to.deep.equal({
                    diceCount: 3,
                    faceCount: 6,
                    modifier: 0
                });
            });

            it('should only override the faceCount when provided', () => {
                const notations = ['d3', '3'];

                notations.forEach((notation) => {
                    expect(dice.diceNotation(notation)).to.deep.equal({
                        diceCount: 1,
                        faceCount: 3,
                        modifier: 0
                    });
                });
            });

            it('should only override the modifier when provided', () => {
                const notations = ['+3', '-3'];

                notations.forEach((notation) => {
                    expect(dice.diceNotation(notation)).to.deep.equal({
                        diceCount: 1,
                        faceCount: 6,
                        modifier: parseInt(notation, 10)
                    })
                });
            });
        });
    });

    describe('numberNotation', () => {
        describe('When given no values', () => {
            it('should return the default values', () => {
                expect(dice.numberNotation()).to.deep.equal({
                    diceCount: 1,
                    faceCount: 6,
                    modifier: 0
                });
            });
        });

        describe('When given values', () => {
            it('should return complete override of defaults if numberNotation is whole', () => {
                expect(dice.numberNotation(2, 7, -4)).to.deep.equal({
                    diceCount: 2,
                    faceCount: 7,
                    modifier: -4
                });
            });

            it('should only override the face count if one value provided', () => {
                expect(dice.numberNotation(10)).to.deep.equal({
                    diceCount: 1,
                    faceCount: 10,
                    modifier: 0
                });
            });

            it('should override face count and dice count if two values provided', () => {
                expect(dice.numberNotation(2, 8)).to.deep.equal({
                    diceCount: 2,
                    faceCount: 8,
                    modifier: 0
                });
            });
        });

    });

    describe('throwDice', () => {
        it('should throw once for each dice', () => {
            expect(dice.throwDice({
                diceCount: 5,
                faceCount: 1,
                modifier: 0
            })).to.equal(5);
        });

        it('should tally up the total dice score', () => {
            sinon.stub(Math, 'random').returns(0.23079);

            expect(dice.throwDice({
                diceCount: 5,
                faceCount: 8,
                modifier: 0
            })).to.equal(10);

            Math.random.restore();
        });

        it('dice rolls should be random', () => {
            // Hard to measure random....so...all the stubs?
            const stub = sinon.stub(Math, 'random');
            stub.onCall(0).returns(0.23079);
            stub.onCall(1).returns(0.45674);

            expect(dice.throwDice({
                diceCount: 1,
                faceCount: 8,
                modifier: 0
            })).to.equal(2);

            expect(dice.throwDice({
                diceCount: 1,
                faceCount: 8,
                modifier: 0
            })).to.equal(4);

            Math.random.restore();
        });

        it('should apply the modifier', () => {
            expect(dice.throwDice({
                diceCount: 1,
                faceCount: 1,
                modifier: 1
            })).to.equal(2);

            expect(dice.throwDice({
                diceCount: 1,
                faceCount: 2,
                modifier: -1
            })).to.equal(1);
        });
    });
});