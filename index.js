'use strict';

const dice = require('./src/dice');

module.exports = {
    roll: dice.roll,
    createRoller: dice.createRoller
};