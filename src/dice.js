'use strict';

// Running on node v5.5.0 for work reasons
// Missing some newer ES6 functionality (destructuring, default args etc)

const merge = require('lodash.merge');

// Regex notation for dice
// See https://jex.im/regulex/#!embed=false&flags=&re=(%5Cd%2Bd)%3F(%5Cd%2B)%3F(%5B%2B-%5D%5Cd%2B)%3F
const NOTATION = /(\d+d)?d?(\d+)?([+-]\d+)?/;
// Dice defaults
const DEFAULTS = {
    diceCount: 1,
    faceCount: 6,
    modifier: 0
};
// Minimum dice value
const MIN = 1;

/**
 * Dice roller class
 * 
 * @class Roller
 */
class Roller {
    /**
     * @constructor
     * @param {Array} diceValues see createRoller
     */
    constructor (diceValues) {
        diceValues = diceValues || [];
        this.diceValues = identifyInput(diceValues[0])(...diceValues);

        // Note this is a slow operation and is not advised
        // Should avoid touching the __proto__ link or setting
        // a prototype to another object
        return Object.setPrototypeOf(() => throwDice(this.diceValues), this);
    }

    /**
     * Converts parsed dice values back to dice notation
     * 
     * @for Roller
     * @method toDiceNotation
     * 
     * @return {String}
     */
    toDiceNotation () {
        const diceCount = this.diceValues.diceCount;
        const faceCount = this.diceValues.faceCount;
        let modifier = this.diceValues.modifier;

        // Need to account for earlier parsing
        modifier = modifier && modifier > 0 ? `+${modifier}` : modifier;

        return `${diceCount}d${faceCount}${modifier || ''}`;
    }
}

/**
 * Creates a new Roller instance and passes on arguments to it
 * 
 * @function createRoller
 * 
 * Parameters can be
 * @param {String} diceNotation e.g. '3d4-2'
 * or any of
 * @param {Number} diceCount
 * @param {Number} faceCount
 * @param {String} modifier
 * 
 * @return {InstanceOf} Roller
 */
function createRoller (/*args*/) {
    return new Roller([...arguments]);
}

/**
 * Rolls dice based on passed parameters or dice notation
 * 
 * @function roll
 * 
 * Parameters can be
 * @param {String} diceNotation e.g. '3d4-2'
 * or any of
 * @param {Number} diceCount
 * @param {Number} faceCount
 * @param {String} modifier
 * 
 * @return {Number}
 */
function roll (/*args*/) {
    const diceValues = identifyInput(arguments[0])(...arguments);
    
    return throwDice(diceValues);
}

/**
 * Identifies the input between dice notation and number notation
 * 
 * @function identifyInput
 * 
 * @param {String | Number} input 
 * @return {Function}
 */
function identifyInput (input) {
    return typeof input === 'string' ? diceNotation : numberNotation;
}

/**
 * Takes a dice notation string and spits out a plain object
 * that overrides DEFAULTS if present
 * 
 * @function diceNotation
 * 
 * @param {String} diceString 
 * @return {Object}
 */
function diceNotation (diceString) {
    const values = NOTATION.exec(diceString);

    return merge({}, DEFAULTS, {
        diceCount: parseInt(values[1], 10) || values[1],
        faceCount: parseInt(values[2], 10) || values[2],
        modifier: parseInt(values[3], 10) || values[3]
    });    
}

/**
 * Takes a series of values and spits out a plain object
 * that overrides DEFAULTS if present
 * If only one number supplied it is the faceCount
 * 
 * @function numberNotation
 * 
 * @param {Number} diceCount 
 * @param {Number} faceCount 
 * @param {String} modifier 
 * @return {Object}
 */
function numberNotation (diceCount, faceCount, modifier) {
    if (arguments.length === 1) {
        faceCount = diceCount;
        diceCount = undefined;
    }

    return merge({}, DEFAULTS, {
        diceCount,
        faceCount,
        modifier: parseInt(modifier, 10) || modifier
    });
}

/**
 * Effectively rolls the dice, for diceCount
 * tallys a random number of faceCount + 1 (dice don't start at 0!)
 * and applies the modifier. If value is less than 1, return 1
 * as I've never seen a dice return a negative number before
 * 
 * @function throwDice
 * 
 * @param {Object} diceValues 
 * @return {Number} diceScore
 */
function throwDice (diceValues) {
    let diceScore = 0;

    for (let i = 0; i < diceValues.diceCount; i++) {
        const diceValue = ((Math.floor(Math.random() * diceValues.faceCount) + MIN) + diceValues.modifier);
        diceScore += diceValue >= MIN ? diceValue : MIN;
    }

    return diceScore;
}

module.exports = {
    Roller,
    createRoller,
    roll,
    identifyInput,
    diceNotation,
    numberNotation,
    throwDice
};